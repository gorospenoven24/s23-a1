// number 3
db.rooms.insertOne({
    "name": "single",
    "accomodates":2,
    "price": 1000,
    "description": " simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
    });

 // Number 4
 db.rooms.insertMany([{
    "name": "double",
    "accomodates":3,
    "price": 2000,
    "description": "A room is fit for a smalll family going on a vacation",
    "rooms_available":5,
    "isAvailable": false
    },
    {
  
    "accomodates":5,
    "price": 4000,
    "description": "A room with a queen size bed perfect for a simple getaway",
    "rooms_available": 15,
    "isAvailable": false
    }
    ]);


 // number 5
 db.rooms.find({
    "name":"double"
    });


 // number 6
 db.rooms.updateOne(
    {"price":4000},
    {
        $set:{
            "rooms_available" : 0
            }
       }
    );

 // number 7
 db.rooms.deleteMany({
    "rooms_available" : 0
});